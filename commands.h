typedef struct json_object json_object;

// defined in main file
void help();
void quit();

// defined elsewhere
void add();
void get();
void edit();
void del();
void keys();
void terminate();
