# eXcrypt - Unix AES password manager

eXcrypt stores all your encrypted data inside a single file which is explicitly specified with an argument: `excrypt {file}`

## Installing

Currently the package is only available on arch linux aur. You can install it with yay or other aur helper:

```
yay -S excrypt
```

On other linux distributions you can install it manually.

## Dependencies

+ **json-c**
+ **openssl** (libssl and libcrypto)

## Build Tools
+ **gcc**
+ **make**

## Building
Just run `make` in your terminal 
