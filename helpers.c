unsigned int strlen(const char *s) 
{
    return((*s)?(strlen(++s)+1):(unsigned int)*s);
}
