#include <stdio.h>
#include <json-c/json.h>
#include "helpers.h"
#include "evp.h"

extern char *path;
extern char *aes_key;
extern size_t key_len;
extern unsigned char iv[16];
extern json_object *parsed_json;
unsigned char ciphertext[100000];

// used only inside THIS file
void write() {
    FILE *fp = fopen(path, "wb");
    const unsigned char *output = json_object_to_json_string(parsed_json);
    encrypt(output, strlen(output), aes_key, iv, ciphertext);
    fwrite(&ciphertext,strlen(ciphertext),1,fp);
    fclose(fp);
}

// used elsewhere
void keys() {
    json_object_object_foreach(parsed_json, key, val) {
        printf("%s\n", key);
    }
}

void add() {
    printf("Enter key: ");
    char buf[4096], key[64], value[4096];
    if (!fgets(buf,64,stdin))
        exit(1);
    sscanf(buf, "%s", &key);
    printf("Enter value: ");
    if (!fgets(buf,4096,stdin))
        exit(1);
    sscanf(buf, "%s", &value);
    
    json_object *json_val = json_object_new_string(value);
    json_object_object_add(parsed_json, key, json_val);
    
    write(parsed_json);
}


void del() {
    printf("Enter key: ");
    char buf[64], key[64];
    if (!fgets(buf,64,stdin))
        exit(1);
    sscanf(buf, "%s", &key);
    json_object *json_el;
    int exists = json_object_object_get_ex(parsed_json, key, &json_el);
    if (exists) {
        json_object_object_del(parsed_json, key);
        write(parsed_json);
    }
    else
        printf("Error: key '%s' doesn't exist\n", key);
}

void get() {
    printf("Enter key: ");
    char buf[64], key[64];
    if (!fgets(buf,64,stdin))
        exit(1);
    sscanf(buf, "%s", &key);
    json_object *json_el;
    int exists = json_object_object_get_ex(parsed_json, key, &json_el);
    if (exists)
        printf("%s\n", json_object_get_string(json_el));
    else
        printf("Error: key '%s' doesn't exist\n", key);
}

void edit() {
    printf("Enter key: ");
    char buf[4096], key[64], value[4096];
    if (!fgets(buf,64,stdin))
        exit(1);
    sscanf(buf, "%s", &key);
    json_object *json_el;
    int exists = json_object_object_get_ex(parsed_json, key, &json_el);
    if (exists) {
        json_object_object_del(parsed_json, key);
        printf("Enter value: ");
        if (!fgets(buf,4096,stdin))
            exit(1);
        sscanf(buf, "%s", &value);
        json_object *json_val = json_object_new_string(value);
        json_object_object_add(parsed_json, key, json_val);
        write(parsed_json);
    }
    else
        printf("Error: key '%s' doesn't exist\n", key);
}

void terminate() {
    printf("WARNING! This action will remove all your file data! Proceed? [y/N] ");
    char action;
    char buf[4096], key[64], value[4096];
    if (!fgets(buf,64,stdin))
        exit(1);
    sscanf(buf, "%c", &action);
    if (action == 'y' || action == 'Y') {
        remove(path);
        exit(1);
    }
}
