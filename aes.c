#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <json-c/json.h>
#include "commands.h"
#include "helpers.h"
#include "evp.h"

struct json_object *parsed_json;
char *argv0;
char *path;
FILE *fp;

char *aes_key;
unsigned char iv[16] = "0000000000000000";

size_t key_len;

typedef struct command {
    char character;
    char description[64];
    void (*func)();
} command;

#define COMMAND_COUNT 8
#define BRUTEFORCE_DELAY 1

command commands[COMMAND_COUNT] = {
    {'a', "add key/value pair", add},
    {'e', "edit key", edit},
    {'d', "delete key", del},
    {'g', "get key", get},
    {'t', "delete this file", terminate},
    {'l', "list keys", keys},
    {'h', "help", help},
    {'q', "quit program", quit}
};

// Most of the commands are defined in commands.c, others are defined below this comment
void help() {
    for (int i = 0; i < COMMAND_COUNT; i++) {
        printf("%c - %s\n", commands[i].character, commands[i].description);
    }
}

void quit() {
    exit(1);
}

char buf[55];

void usage(void) {
    printf("usage: %s {path} [-h help] [-v version]\n", argv0);
    exit(0);
}

void input_newline()
{
    printf("(%s) ", argv0);
}

int main(int argc, char *argv[])
{
    for (argv0 = *argv, argv++, argc--; argv[0]; argc--, argv++) {
        if (argv[0][0] == '-') {
            char argc_;
            if (argv[0][1] == '\0')
                usage(); 
            int i_;
            for (int i_ = 1; argv[0][i_]; i_++) {
                argc_ = argv[0][i_];
                switch (argc_) {
                    case 'v':
                        printf("%s 0.0.2", argv0);
                        exit(0);
                    default:
                        usage();
                }
            }
        } else {
            if (path)
                usage();
            else {
                path = argv[0];
            } }
    }
    if (!path) usage();

    if (access(path, F_OK) != -1) {
        struct stat path_stat;
        stat(path, &path_stat);

        if (!S_ISREG(path_stat.st_mode)) {
            unsigned int path_len = strlen(path);
            if (path[path_len - 1] != '/') {
                path[path_len] = '/';
                path[path_len + 1] = '\0';
                path_len++;
            }

            char action;
            char filename[] = "secret.ef";
            for (int i = path_len; i < path_len + 11; i++) 
                path[i] = filename[i - path_len];

            if (access(path, F_OK) == -1) {
                printf("Specified path is a folder. Proceed with creating secret.ef file in the folder? [Y/n] ");

                if (fgets(buf,55,stdin)) {
                    sscanf(buf, "%c", &action);

                    if (action == 'N' || action == 'n') 
                       return 1;
                    else {
                        fp = fopen(path, "w");
                        fclose(fp);
                    }
                } else return 0;
            }
        }

        fp = fopen(path, "rb");
        unsigned char *buff;

        fseek( fp , 0L , SEEK_END);
        long fp_size = ftell(fp);
        rewind(fp);

        buff = calloc(1, fp_size+1);
        fread(buff,fp_size,1,fp);
        fclose(fp);

        unsigned char *decrypted;
        decrypted = calloc(1, fp_size+1);
        int code;
        while (1) {
            aes_key = getpass("Enter key (16 characters only!): ");
            key_len = strlen(aes_key);
            
            sleep(BRUTEFORCE_DELAY);
            if (key_len != 16) {
                printf("Incorrect key! \n"); 
                exit(0);
            }
            code = decrypt(buff, strlen(buff), aes_key,iv, decrypted);
            if (code < 0) {
                if (!decrypted[0]) {
                     parsed_json = json_object_new_object();
                     break;
                }
                else {
                    printf("Wrong password or corrupted file \n");
                    exit(0);
                }
            } else {
                parsed_json = json_tokener_parse(decrypted);
                break;
            }
        }

        

        printf("Available commands:\n");
        help(parsed_json);

        int command_found;
        char input;
        while (1) {
            command_found = 0;
            input_newline();
            if (!fgets(buf, 55, stdin))
                return 0;
            sscanf(buf, "%c", &input);
            for (int i = 0; i < COMMAND_COUNT; i++) {
                if (input == commands[i].character) {
                    commands[i].func();
                    command_found = 1;
                    break;
                }
            }
            if (!command_found) printf("Command not found. h - list all available commands\n");
        }
    } else {
        printf("Error: incorrect file/directory path\n");
    }
    return 0;
}
